import NextAuth, { AuthOptions } from 'next-auth'
// import Auth0Provider from 'next-auth/providers/auth0'
import CredentialsProvider from 'next-auth/providers/credentials'
// import FacebookProvider from 'next-auth/providers/facebook'
// import GithubProvider from 'next-auth/providers/github'
// import GoogleProvider from 'next-auth/providers/google'
// import TwitterProvider from 'next-auth/providers/twitter'

export const authOptions: AuthOptions = {
  session: {
    strategy: 'jwt',
    // Giây - Thời gian cho đến khi phiên không hoạt động hết hạn và không còn hiệu lực nữa.
    maxAge: 24 * 60 * 60, // 1 ngày
  },
  // Cấu hình một hoặc nhiều nhà cung cấp xác thực
  providers: [
    CredentialsProvider({
      // Tên được hiển thị trên biểu mẫu đăng nhập (ví dụ: "Đăng nhập bằng...")
      name: 'Credentials',
      // `credentials` được sử dụng để tạo ra một biểu mẫu trên trang đăng nhập.
      // Bạn có thể chỉ định các trường nào sẽ được gửi, bằng cách thêm các khóa vào đối tượng `credentials`.
      // ví dụ: domain, username, password, 2FA token, v.v.
      // Bạn có thể chuyển bất kỳ thuộc tính HTML nào cho thẻ <input> thông qua đối tượng này.
      credentials: {
        email: { label: 'Email', type: 'email', placeholder: 'Vui lòng nhập email của bạn' },
        password: {
          label: 'Mật khẩu',
          type: 'password',
          placeholder: 'Vui lòng nhập mật khẩu của bạn',
        },
      },
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      async authorize(credentials, req) {
        // Bạn cần cung cấp logic của riêng bạn ở đây, lấy các thông tin đăng nhập
        // được gửi và trả về một đối tượng biểu diễn người dùng hoặc giá trị
        // là false/null nếu thông tin đăng nhập không hợp lệ.
        // ví dụ: return { id: 1, name: 'J Smith', email: 'jsmith@example.com' }
        // Bạn cũng có thể sử dụng đối tượng `req` để lấy thêm các tham số
        // (ví dụ: địa chỉ IP của yêu cầu)
        // const res = await fetch('http://localhost:8000/auth/login', {
        //   method: 'POST',
        //   headers: {
        //     'Content-Type': 'application/json',
        //   },
        //   body: JSON.stringify({
        //     username: credentials?.username,
        //     password: credentials?.password,
        //   }),
        // })
        // const user = await res.json()

        const user = {
          id: '1',
          // email: credentials?.email,
          email: 'admin@gmail.com',
          name: 'admin123',
        }

        // Nếu không có lỗi và chúng ta có dữ liệu người dùng, trả về nó
        // if (res.ok && user)
        if (user) {
          // Bất kỳ đối tượng nào được trả về sẽ được lưu trong thuộc tính `user` của JWT
          return user
        }
        // Nếu bạn trả về null thì sẽ hiển thị một thông báo lỗi yêu cầu người dùng kiểm tra chi tiết đăng nhập của họ.
        return null
        // Bạn cũng có thể từ chối cuộc gọi này với một Lỗi nếu người dùng sẽ được chuyển đến trang lỗi với thông báo lỗi như một tham số truy vấn
      },
    }),
    // FacebookProvider({
    //   clientId: process.env.FACEBOOK_ID as string,
    //   clientSecret: process.env.FACEBOOK_SECRET as string,
    // }),
    // GithubProvider({
    //   clientId: process.env.GITHUB_ID as string,
    //   clientSecret: process.env.GITHUB_SECRET as string,
    // }),
    // GoogleProvider({
    //   clientId: process.env.GOOGLE_ID as string,
    //   clientSecret: process.env.GOOGLE_SECRET as string,
    // }),
    // TwitterProvider({
    //   clientId: process.env.TWITTER_ID as string,
    //   clientSecret: process.env.TWITTER_SECRET as string,
    // }),
    // Auth0Provider({
    //   clientId: process.env.AUTH0_ID as string,
    //   clientSecret: process.env.AUTH0_SECRET as string,
    //   issuer: process.env.AUTH0_ISSUER,
    // }),
  ],
  theme: {
    colorScheme: 'light',
  },
  callbacks: {
    async jwt({ token, user, account }) {
      // eslint-disable-next-line no-console
      console.log({ account })
      // Lưu trữ access_token OAuth và/hoặc id người dùng vào token ngay sau khi đăng nhập
      // if (account) {
      //   token.accessToken = account.access_token
      //   token.id = profile.id
      // }

      return { ...token, ...user }
    },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async session({ session, token, user }) {
      // Gửi các thuộc tính đến máy khách, như access_token và id người dùng từ một nhà cung cấp.
      // session.accessToken = token.accessToken
      // session.user.id = token.id
      return session // Kiểu trả về sẽ phù hợp với kiểu trả về trong `useSession()`
    },
  },
  pages: {
    signIn: '/auth/login', // trang đăng nhập tùy chỉnh
  },
}

export default NextAuth(authOptions)
