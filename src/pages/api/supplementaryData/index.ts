// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

import { supplementaryDataList } from 'constants/SupplementaryData.constant'
import { SupplementaryData } from 'types/supplementaryData.type'

export default function handler(req: NextApiRequest, res: NextApiResponse<SupplementaryData[]>) {
  res.status(200).json(supplementaryDataList)
}
