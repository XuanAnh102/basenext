import { NextApiRequest, NextApiResponse } from 'next'

import { supplementaryDataList } from 'constants/SupplementaryData.constant'
import { SupplementaryData } from 'types/supplementaryData.type'

export default function handler(req: NextApiRequest, res: NextApiResponse<SupplementaryData[]>) {
  res.status(200).json(supplementaryDataList)
}
