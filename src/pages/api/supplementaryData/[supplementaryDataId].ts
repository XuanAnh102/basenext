import { NextApiRequest, NextApiResponse } from 'next'

import { supplementaryDataList } from 'constants/SupplementaryData.constant'

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const { supplementaryDataId } = req.query
  if (supplementaryDataId) {
    switch (req.method) {
      case 'GET':
        // eslint-disable-next-line no-case-declarations
        const supplementaryData = supplementaryDataList.find(
          (item) => item.id === +supplementaryDataId
        )
        res.status(200).json(supplementaryData ?? null)
        break
      case 'POST':
        res.status(200).json({})
        break
      case 'PUT':
        res.status(200).json({})
        break

      default:
        break
    }
  } else {
    res.status(500).json({ error: 'failed to load data' })
  }
}
