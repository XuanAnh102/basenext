import { Form, Input, Select, Space, Switch } from 'antd'
import type { GetStaticProps, InferGetStaticPropsType } from 'next'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { signIn } from 'next-auth/react'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import React, { useState } from 'react'

import { ClapSpinner } from 'components/ui/spinners'

const { Option } = Select

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale ?? 'en', ['login'])),
  },
})

const Login = (_props: InferGetStaticPropsType<typeof getStaticProps>) => {
  const router = useRouter()
  const { t } = useTranslation('login')
  const [isLoading, setLoading] = useState<boolean>(false)

  const onSubmit = async (values: any) => {
    try {
      setLoading(true)
      const response = await signIn('credentials', {
        username: values.email,
        password: values.password,
        redirect: false,
      })

      if (response && response.ok) {
        setTimeout(() => {
          router.replace('/')
          setLoading(false)
        }, 1500)
      }
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log('Login error: ', error)
      setLoading(false)
    }
  }

  return (
    <section className='flex min-h-screen items-stretch text-white '>
      <div
        className='relative hidden w-1/2 items-center bg-gray-500 bg-cover bg-no-repeat lg:flex'
        style={{
          backgroundImage:
            'url(https://funix.edu.vn/wp-content/uploads/2021/12/chuyen-doi-so-trong-hoat-dong-cua-co-quan-nha-nuoc-nhu-the-nao.jpeg)',
        }}
      >
        <div className='absolute inset-0 z-0 bg-black opacity-30' />
        <div className='z-10 w-full px-24'>
          <h1 className='text-left text-5xl font-bold tracking-wide'>
            {t('Keep it special', { ns: 'login' })}
          </h1>
          <p className='my-4 text-3xl'>
            {t('Capture your personal memory in unique way, anywhere.', { ns: 'login' })}
          </p>
        </div>
        <div className='absolute bottom-0 left-0 right-0 flex justify-center space-x-4 p-4 text-center'>
          <span>
            <svg
              fill='#fff'
              xmlns='http://www.w3.org/2000/svg'
              width={24}
              height={24}
              viewBox='0 0 24 24'
            >
              <path d='M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z' />
            </svg>
          </span>
          <span>
            <svg
              fill='#fff'
              xmlns='http://www.w3.org/2000/svg'
              width={24}
              height={24}
              viewBox='0 0 24 24'
            >
              <path d='M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z' />
            </svg>
          </span>
          <span>
            <svg
              fill='#fff'
              xmlns='http://www.w3.org/2000/svg'
              width={24}
              height={24}
              viewBox='0 0 24 24'
            >
              <path d='M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.26.014 3.668.073 4.948.2 4.362 2.622 6.78 6.98 6.98 1.281.059 1.689.073 4.947.073 3.26 0 3.668-.014 4.948-.073 4.363-.2 6.78-2.622 6.98-6.98.059-1.281.073-1.689.073-4.948 0-3.26-.014-3.668-.073-4.948-.2-4.363-2.622-6.78-6.98-6.98-1.28-.058-1.688-.072-4.948-.072zm5.824 17.944c-.791.791-1.84 1.233-2.949 1.233-1.108 0-2.157-.442-2.947-1.233-.791-.791-1.233-1.84-1.233-2.949 0-1.108.442-2.157 1.233-2.948.791-.791 1.84-1.233 2.947-1.233 1.109 0 2.158.442 2.949 1.233.791.791 1.233 1.84 1.233 2.948 0 1.109-.442 2.158-1.233 2.949z' />
            </svg>
          </span>
        </div>
      </div>
      <div
        className='z-0 flex w-full items-center justify-center px-0 text-center md:px-16 lg:w-1/2'
        style={{
          backgroundColor: '#f3efef',
        }}
      >
        <div className='z-20 w-full py-6'>
          {/* Add the title here */}
          <h1 style={{ color: '#2563EB' }} className='mb-4 text-3xl font-bold '>
            {t('Đăng nhập', { ns: 'login' })}
          </h1>
          {/* Form content */}
          <Form
            name='loginForm'
            layout='vertical'
            className='mx-auto mt-10 w-full px-4 text-left sm:w-2/3 lg:px-0'
            initialValues={{ email: '', password: '' }}
            onFinish={onSubmit}
            autoComplete='off'
          >
            {/* E-mail */}
            <Form.Item
              label='E-mail'
              name='email'
              rules={[{ required: true, message: 'Please input your email!' }]}
            >
              <Input />
            </Form.Item>

            {/* Password */}
            <Form.Item
              label='Mật khẩu'
              name='password'
              rules={[{ required: true, message: 'Please input your password!' }]}
            >
              <Input.Password />
            </Form.Item>

            {/* Forgot Password */}
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <Form.Item>
                <Link href='/forgot-password' passHref>
                  <span className='text-blue-600 hover:underline'>
                    {t('Quên mật khẩu', { ns: 'login' })}
                  </span>
                </Link>
              </Form.Item>

              {/* Remember Me */}
              <Form.Item>
                <Space>
                  <span>{t('Duy trì đăng nhập', { ns: 'login' })}</span>
                  <Switch />
                </Space>
              </Form.Item>
            </div>

            {/* Submit Button */}
            <Form.Item>
              <button
                type='submit'
                className='relative flex w-full items-center justify-center rounded-md bg-blue-600 px-4 py-2 text-white transition-colors duration-300 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-opacity-50'
              >
                <span className='relative z-10'>
                  {isLoading ? 'Đang xử lý...' : t('Đăng nhập', { ns: 'login' })}
                </span>
                {isLoading && (
                  <div className='absolute inset-0 flex items-center justify-center'>
                    <ClapSpinner size={12} />
                  </div>
                )}
              </button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </section>
  )
}

export default Login
